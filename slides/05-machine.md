### Docker для продолжающих

<small>Created by [Valerii Kravets](viniokil@gmail.com) / [@viniokil](https://t.me/viniokil)</small>

---

![logo](images/docker-official.svg)

## Docker-machine

---

## Для чего стоит использовать Dockerfile?

---

## Какие инструкции Dockerfile пригодились?

---

## В чем разница между `ENTRYPOINT` и `CMD`

---

## Какие проблемы 
## решает docker-compose?

---

## Когда стоит использовать 
## docker-compose.yml?

---

## Какие директивы 
## docker-compose.yml обязятельны?

---

### Какие виды монтирования 
### можно использовать в блоке
`volumes:`

---

### Приведите пример использования

- image
- env
- env_file
- command
- networks

---

![docker_machine.png](images/docker_machine.png)

---

### Что такое Docker Machine?

Утилита для управления удаленным Docker Engine

---

### Docker Machine используется для:

* Установки и запуска Docker на Mac или Windows
* Установка, настройка и управление множества удаленных Docker хостов
* Установка, настройка и управление Swarm кластерами

---

## docker-machine create

Для создания docker хоста

---

#### Поддерживаемые сервисы

* Amazon Web Services
* Microsoft Azure
* Digital Ocean
* Google Compute Engine
* Generic - **Любая Linux машина**
* ...

[Полный список](https://docs.docker.com/machine/drivers/)

---

#### Для занятия понадобится доступ к Google Cloud Platform

[Скачать gce-credentials.json](https://gitlab.com/viniokil/docker-course/raw/master/docker/gce-credentials.json)

Перенесите его в домашний каталог

#### или

```shell
curl https://gitlab.com/viniokil/docker-course/raw/master/docker/gce-credentials.json -o ~/gce-credentials.json
```

---

#### Обьявим путь к ключю через переменную окружения

```shell
export GOOGLE_APPLICATION_CREDENTIALS=$HOME/gce-credentials.json
```

---

### Создаем докер машину

```shell
docker-machine create \
            `# указываем провайдера` \
            --driver google \
            `# для GCP - обязательно название проекта` \
            --google-project helical-fin-239509 \
            `# имя виртуальной машины` \
            vm-$(whoami)
```

---

### Смотрим спискок своих машинок

```shell
docker-machine ls         
```

```
NAME      ACTIVE   DRIVER   STATE     URL                          SWARM   DOCKER     ERRORS
vm-vinz   -        google   Running   tcp://104.154.244.128:2376           v18.09.2
```

---

### Подключаемся к докер хосту

```shell
docker-machine ssh vm-$(whoami)
```

---
### И он не идеален

**Старая Ubuntu 16.04**

```shell
Welcome to Ubuntu 16.04.2 LTS (GNU/Linux 4.10.0-27-generic x86_64)

 * Documentation:  https://help.ubuntu.com
 * Management:     https://landscape.canonical.com
 * Support:        https://ubuntu.com/advantage

  Get cloud support with Ubuntu Advantage Cloud Guest:
    http://www.ubuntu.com/business/services/cloud

214 packages can be updated.
123 updates are security updates.

New release '18.04.1 LTS' available.
```

---

### И еще недостатки:
- Ubuntu 16.04
- мало/много диска
- дорого, как для тестов
- HDD, а нам бы SSD
- закрыты http и https порты
---

### Починим безобразие

Как? Удалим кривой инстанс

```shell
docker-machine rm vm-$(whoami)
```

---

### Находим имейдж посвежее

Необходим набор утилит [Cloud SDK](https://cloud.google.com/sdk/)

```shell
gcloud compute images list --uri | grep ubuntu
```

```
https://www.googleapis.com/compute/v1/projects/ubuntu-os-cloud/global/images/ubuntu-1404-trusty-v20190209
https://www.googleapis.com/compute/v1/projects/ubuntu-os-cloud/global/images/ubuntu-1604-xenial-v20190212
https://www.googleapis.com/compute/v1/projects/ubuntu-os-cloud/global/images/ubuntu-1804-bionic-v20190212a
https://www.googleapis.com/compute/v1/projects/ubuntu-os-cloud/global/images/ubuntu-1810-cosmic-v20190212
https://www.googleapis.com/compute/v1/projects/ubuntu-os-cloud/global/images/ubuntu-minimal-1604-xenial-v20190212
https://www.googleapis.com/compute/v1/projects/ubuntu-os-cloud/global/images/ubuntu-minimal-1804-bionic-v20190212
https://www.googleapis.com/compute/v1/projects/ubuntu-os-cloud/global/images/ubuntu-minimal-1810-cosmic-v20190212
```

---

### Добавим полезных флагов

```shell

--google-disk-size 25 # Размер диска 25 Gb
--google-disk-type # Тип диска - SSD
--google-preemptible # Инстанс, который будет выключен через <24 часа
--google-machine-image \
    https://www.googleapis.com/compute/v1/projects/ubuntu-os-cloud/global/images/ubuntu-1804-bionic-v20190212a
--google-tags http,https # Теги инстанса GCP чтобы разрешить 80 и 443 порт. 
                         # Остальные закрыты
```

---

### Список доступных флагов доступен по ссылке

[docker-machime GCP](https://docs.docker.com/machine/drivers/gce/)

---

### Запускаем интанс как надо

```bash
docker-machine create \
            `# указываем провайдера` \
            --driver google \
            `# для GCP - обязательно название проекта` \
            --google-project helical-fin-239509 \
            `# Размер диска 25 Gb` \
            --google-disk-size 25 \
            `# Тип диска - SSD` \
            --google-disk-type pd-ssd \
            `# Инстанс, который будет выключен через <24 часа` \
            --google-preemptible \
            `# Теги инстанса GCP чтобы разрешить 80 и 443 порт. Остальные закрыты` \
            --google-tags http,https \
            `# Обарз Ubuntu 18.04 для виртуалки` \
            --google-machine-image \
    https://www.googleapis.com/compute/v1/projects/ubuntu-os-cloud/global/images/ubuntu-1804-bionic-v20190212a \
            `# имя виртуальной машины` \
            vm-$(whoami)
```

---

# И как это работает?

---

* Создается инстанс
* SSH соединение
* Устанавливается Docker на инстанс
* Передаются сертификаты безопасности на сервер и с него
* Настраивается Docker демон на удаленное подключение

---

### Локально создается такая структура файлов

```shell
/home/user/.docker/
├── config.json
└── machine
    ├── certs
    │   ├── ca-key.pem
    │   ├── ca.pem
    │   ├── cert.pem
    │   └── key.pem
    └── machines
        └── vm-vinz # Имя виртуальной машины
            ├── ca.pem
            ├── cert.pem
            ├── config.json # описывает как подключиться к docker machine
            ├── id_rsa # SSH ключ для подключения
            ├── id_rsa.pub
            ├── key.pem
            ├── server-key.pem
            └── server.pem
```

---

# В чем сила?

---

### В управлении удаленной машиной, как своей

```shell
docker-machine env vm-$(whoami)

export DOCKER_TLS_VERIFY="1"
export DOCKER_HOST="tcp://104.154.244.128:2376"
export DOCKER_CERT_PATH="/home/vinz/.docker/machine/machines/vm-vinz"
export DOCKER_MACHINE_NAME="vm-vinz"
# Run this command to configure your shell: 
# eval $(docker-machine env vm-vinz)
```

---

### Настраиваем работу с удаленной машиной

```bash
eval $(docker-machine env vm-vinz)
```

---

### Запустип что-то простое


```
docker run --rm -p 80:80 -d nginx
```

---

### А где посмотреть?
```
docker-machine ip vm-$(whoami)
```
```
104.154.244.128
```

---

```yml
version: '3'

networks:
  backend-network:
    driver: bridge

services:

  &DB_HOST db:
    image: mysql:5.7
    restart: always
    environment:
      MYSQL_USER: &DB_USER exampleuser # задаем переменную DB_USER
      MYSQL_PASSWORD: &DB_PASSWORD examplepass # --//--
      MYSQL_DATABASE: &DB_NAME exampledb # == WORDPRESS_DB_NAME
      MYSQL_RANDOM_ROOT_PASSWORD: '1' # рандомный рут пароль
    networks:
      - backend-network

  wordpress:
    image: wordpress
    restart: always
    ports:
      - 8888:80
    environment:
      WORDPRESS_DB_HOST: *DB_HOST # совпадает с названием сервиса c БД
      WORDPRESS_DB_USER: *DB_USER # совпадает с  MYSQL_USER
      WORDPRESS_DB_PASSWORD: *DB_PASSWORD # MYSQL_PASSWORD
      WORDPRESS_DB_NAME: *DB_NAME # MYSQL_DATABASE
    networks:
      - backend-network

  adminer:
    image: adminer
    restart: always
    ports:
      - 8080:8080
    networks:
      - backend-network
```

---

### Как сделать все доступным на 80 порту?

---

## Натравить хомячка Traefik

![traefik.png](images/traefik.png)

---

## Что за зверь

* Обратный реверс прокси
* Как nginx


---

### Как готовить

```yml
networks:
...
services:
  # Реверс прокси (Traefik)
  traefik:
    image: traefik  # официальный Docker image
    command: --api --docker  # Включаем web UI
                             # и говорим Traefik слушать docker
    ports:
      - "80:80"      # HTTP порт
      - "8080:8080"  # Web UI (включен --api)
    volumes:
      - /var/run/docker.sock:/var/run/docker.sock
      # Слушаем события в докере
    networks:
      - traefik
```

---

```shell
  docker network create traefik
```

---

```yml

networks:
  backend-network:
    driver: bridge
  traefik:
    external: true # использовать существующую сеть

services:
  # Реверс прокси (Traefik)
  traefik:
    image: traefik  # официальный Docker image
    ...
    volumes:
      - /var/run/docker.sock:/var/run/docker.sock
    networks:
      - backend-network
      - traefik
```

---

### Готовим wordpress для traefik

```yml
  wordpress:
    ...
    # ports:
    #   - 8888:80
    labels: # метки
      - "traefik.docker.network=traefik" # Имя общей сети Traefik и Wordpress
      - traefik.port=80 # Какой порт слушать трифику
      - 'traefik.enable=true' # Трифик - обрати внимание на этот сервис
      - "traefik.frontend.rule=Host:wordpress.localhost" # присваисваем домен для сервиса
    networks:
      - backend-network
      - traefik
```

---

```yml
version: '3'

networks:
  backend-network:
    driver: bridge
  traefik:
    external: true # использовать существующую сеть

volumes: # Обьявляем о существовании томов
  home-dir: # Название вольюма

services:
  &DB_HOST db:
    image: mysql:5.7
    restart: always
    volumes:
      - ./mysql:/var/lib/mysql # Монтируем папку контейнера /var/lib/mysql
                               # в локальную папку по пути ./mysql
    environment:
      MYSQL_USER: &DB_USER exampleuser # задаем переменную DB_USER
      MYSQL_PASSWORD: &DB_PASSWORD examplepass # --//--
      MYSQL_DATABASE: &DB_NAME exampledb # == WORDPRESS_DB_NAME
      MYSQL_RANDOM_ROOT_PASSWORD: '1' # рандомный рут пароль
    env_file:
      - .env

    networks:
      - backend-network

  wordpress:
    image: wordpress
    restart: always
    # ports:
    #   - 8888:80
    volumes:
      - ./wordpress:/var/www/html
      - home-dir:/root # Монтируем папку контейнера /root
                       # в локальную docker volume home-dir
    environment:
      WORDPRESS_DB_HOST: *DB_HOST # совпадает с названием сервиса c БД
      WORDPRESS_DB_USER: *DB_USER # совпадает с  MYSQL_USER
      WORDPRESS_DB_PASSWORD: *DB_PASSWORD # MYSQL_PASSWORD
      WORDPRESS_DB_NAME: *DB_NAME # MYSQL_DATABASE
    labels: # метки
      - "traefik.docker.network=traefik" # Имя общей сети Traefik и Wordpress
      - traefik.port=80 # Какой порт слушать трифику
      - 'traefik.enable=true' # Трифик - обрати внимание на этот сервис
      - "traefik.frontend.rule=Host:wordpress.localhost" # присваисваем домен для сервиса
    networks:
      - backend-network
      - traefik

  adminer:
    image: adminer
    restart: always
    # ports:
    # - 8080:8080
    labels:
      - "traefik.docker.network=traefik"
      - traefik.port=8080
      - 'traefik.enable=true'
      - "traefik.frontend.rule=Host:adminer.localhost"
    networks:
      - backend-network
      - traefik

  # Реверс прокси (Traefik)
  traefik:
    image: traefik  # официальный Docker image
    command: --api --docker  # Включаем web UI
                             # и говорим Traefik слушать docker
    ports:
      - "80:80"      # HTTP порт
      - "8080:8080"  # Web UI (включен --api)
    volumes:
      - /var/run/docker.sock:/var/run/docker.sock
      # Слушаем события в докере
    networks:
      - traefik

```

---

### Удалить инстанс

```shell
docker-machine rm vm-$(whoami)
```

---

# ДЗ

---

* Зарегистрироваться на [GCP](https://console.cloud.google.com/compute)
* Поиметь $300
Вам поможет ссылка в описании [$300 GCP](https://docs.google.com/document/d/14IVBrFEwHC1GEaCiC1N8ZQWqbkjG3gQEkjK7B3YjXOg/edit)
* Создать проект на GCP

---

* Установить [Cloud SDK](https://cloud.google.com/sdk/)
* Выполнить `gcloud auth login`
* Выполнить `gcloud config set project PROJECT_ID`
* Выполнить `gcloud auth application-default login`

---

* Создать docker-machine используя флаг `--google-tags `
* На ней запустить `docker run -p 80:8080 sdinhbsm/go-ping`
* Ввести в браузере ip докер машины
* остановить и удалить контейнер go ping

---

* Зарегистрировать себе домен на [freenom.com](https://www.freenom.com/ru/index.html)
* Направить A запись домена на IP docker machine

---

### Написать docker-compose.yml который

* Включает [sdinhbsm/go-ping](https://hub.docker.com/r/sdinhbsm/go-ping/tags)
* Использует для https [https-portal](https://github.com/SteveLTN/https-portal)
* Включает **Portainer**
* Имеет коментарии для нового

---

![tochno.jpg](images/tochno.jpg)

---

```yml
version: '3'

services:

  https-portal:
    container_name: https-portal
    image: steveltn/https-portal:latest
    depends_on:
    - portainer
    ports:
      - '80:80'
      - '443:443'
    volumes:
      - /var/run/docker.sock:/var/run/docker.sock:ro
      - /srv/docker/https-portal:/var/lib/https-portal
    environment:
      WEBSOCKET: 'true'
      CLIENT_MAX_BODY_SIZE: '0M'
      DOMAINS: 'd.vinz.top -> http://portainer:9000'
      STAGE: production
#      ACCESS_LOG: 'on'
#      FORCE_RENEW: 'true'

  portainer:
    container_name: portainer
    image: portainer/portainer
    command: -H unix:///var/run/docker.sock
#    ports:
#      - 9000:9000
    volumes:
      - /var/run/docker.sock:/var/run/docker.sock
      - /srv/docker/portainer:/data

```