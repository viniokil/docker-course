### Docker для продолжающих

<small>Created by [Valerii Kravets](viniokil@gmail.com) / [@viniokil](https://t.me/viniokil)</small>

---

![logo](images/docker-official.svg)

## Troubleshooting

---

# ДЗ

---

* Зарегистрироваться на [GCP](https://console.cloud.google.com/compute)
* Поиметь $300
Вам поможет ссылка в описании [$300 GCP](https://docs.google.com/document/d/14IVBrFEwHC1GEaCiC1N8ZQWqbkjG3gQEkjK7B3YjXOg/edit)
* Создать проект на GCP

---

* Установить [Cloud SDK](https://cloud.google.com/sdk/)
* Выполнить `gcloud auth login`
* Выполнить `gcloud config set project PROJECT_ID`
* Выполнить `gcloud auth application-default login`

---

* Создать docker-machine используя флаг `--google-tags `
* На ней запустить `docker run -p 80:8080 sdinhbsm/go-ping`
* Ввести в браузере ip докер машины
* остановить и удалить контейнер go ping

---

* Зарегистрировать себе домен на [freenom.com](https://www.freenom.com/ru/index.html)
* Направить A запись домена на IP docker machine

---

---

## Что такое troubleshooting

---

### Цель

- работающий web сервис
- в Docker
- защищенный ssl сертификатом
- доступный по домену

---

|          |                     |
| ---------| ------------------- |
| Сервер   | docker-machine GCP  |
| IP       | 34.66.80.64         |
| Login    | ubuntu              |
| Password | qwerty1234          |

---

![site-not-work](../images/site-not-work.jpg)

---

d.vinz.top

⇓ dns ⇓

34.66.80.64

⇓ port ⇓

http  | 80

https | 443

⇓ server ⇓

docker

⇓ service ⇓

adminer | mysql

---

### Пробуем зайти на d.vinz.top

В браузере

или

```shell
curl d.vinz.top
```

---

![ERR_CONNECTION_TIMED_OUT](../images/ERR_CONNECTION_TIMED_OUT.png)

---

### Проверяем DNS

```shell
host d.vinz.top
```

```error
Host d.vinz.top not found: 3(NXDOMAIN)
```

---

### Значит не задана A запись DNS

---

### Запись применяется не сразу

---

### Пытаемся зайти по IP

```shell
curl 34.66.80.64
```

### Тоже зависло

---

### Але, а как же ping

```shell
ping 34.66.80.64
```

```shell
PING 34.66.80.64 (34.66.80.64) 56(84) bytes of data.
64 bytes from 34.66.80.64: icmp_seq=1 ttl=52 time=144 ms
```

---

### Пинг не работает с AWS инстансами

---

<span style="color:green">d.vinz.top</span>

<span style="color:green">⇓ dns ⇓</span>

<span style="color:green">34.66.80.64</span>

⇓ port ⇓

http  | 80

https | 443

⇓ server ⇓

docker

⇓ service ⇓

adminer | mysql

---

### Проверяем порт 80

```shell
nc -vz 34.66.80.64 80
```

```shell
```

---

### Мы хоть к ssh приконектимся

```shell
nc -vz 34.66.80.64 22
```

```shell
Connection to 34.66.80.64 22 port [tcp/ssh] succeeded!
SSH-2.0-OpenSSH_7.6p1 Ubuntu-4ubuntu0.2
```

---

### Заходим на сервак по SSH

```shell
ssh ubuntu@34.66.80.64
```

---

## PASS: qwerty1234

---

### А если лень постоянно вводить пароль

```shell
ssh-copy-id ubuntu@34.66.80.64
```

**PASS: qwerty1234**

---

### И сразу решить вопрос с безопасностью 

Отключить парольный SSH

```shell
sudo nano /etc/ssh/sshd_config
```

<span style="color:red">PasswordAuthentication yes</span>

<span style="color:green">PasswordAuthentication no</span>

---

#### Проверяем работает на 80 порте

```shell
sudo lsof -i :80
```

```
google_ne 1058 root    7u  IPv4  47439      0t0  TCP vm-vinz.c.glassy-vial-205520.internal:38898->metadata.google.internal:http (ESTABLISHED)
google_cl 1059 root    5u  IPv4  47440      0t0  TCP vm-vinz.c.glassy-vial-205520.internal:38900->metadata.google.internal:http (ESTABLISHED)
google_cl 1059 root    6u  IPv4  47415      0t0  TCP vm-vinz.c.glassy-vial-205520.internal:38896->metadata.google.internal:http (CLOSE_WAIT)
google_ac 1070 root    5u  IPv4  47454      0t0  TCP vm-vinz.c.glassy-vial-205520.internal:38902->metadata.google.internal:http (ESTABLISHED)
```

---

### А где вебсервер

* nginx
* apache
* сервис в docker

---

# A нет

---

#### Смотрим в домашнюю папку

Вдруг кто оставил что-то полезное

```shell
ls -alh
```

```shell
ubuntu@vm-vinz:~$ ls
docker-compose.yml
```

---
