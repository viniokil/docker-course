### Docker для продолжающих

<small>Created by [Valerii Kravets](viniokil@gmail.com) / [@viniokil](https://t.me/viniokil)</small>

---

![logo](images/docker-official.svg)

## Docker-compose

---

## Вопросы 

---

## Осторожно - много букв

---

```yml
version: '3'

services:

  &DB_HOST db:
    image: mysql:5.7
    restart: always
    environment:
      MYSQL_USER: &DB_USER exampleuser # задаем переменную DB_USER
      MYSQL_PASSWORD: &DB_PASSWORD examplepass # --//--
      MYSQL_DATABASE: &DB_NAME exampledb # == WORDPRESS_DB_NAME
      MYSQL_RANDOM_ROOT_PASSWORD: '1' # рандомный рут пароль

  wordpress:
    image: wordpress
    restart: always
    ports:
      - 8888:80
    environment:
      WORDPRESS_DB_HOST: *DB_HOST # совпадает с названием сервиса c БД
      WORDPRESS_DB_USER: *DB_USER # совпадает с  MYSQL_USER
      WORDPRESS_DB_PASSWORD: *DB_PASSWORD # MYSQL_PASSWORD
      WORDPRESS_DB_NAME: *DB_NAME # MYSQL_DATABASE

  adminer:
    image: adminer
    restart: always
    ports:
      - 8080:8080
```

---

### Как все потерять с таким конфигом? 

```shell
docker-compose down -v
```

---

### Что хранить?

* Данные приложения
* Базу данных

---

```yml
  &DB_HOST db:
    image: mysql:5.7
    restart: always
    environment:
      MYSQL_USER: &DB_USER exampleuser # задаем переменную DB_USER
      MYSQL_PASSWORD: &DB_PASSWORD examplepass # --//--
      MYSQL_DATABASE: &DB_NAME exampledb # == WORDPRESS_DB_NAME
      MYSQL_RANDOM_ROOT_PASSWORD: '1' # рандомный рут пароль
    volumes:
      - ./mysql:/var/lib/mysql # Монтируем папку контейнера /var/lib/mysql
                               # в локальную папку по пути ./mysql
```

---

```yml
  wordpress:
    image: wordpress
    restart: always
    ports:
      - 8888:80
    environment:
      WORDPRESS_DB_HOST: *DB_HOST # совпадает с названием сервиса c БД
      WORDPRESS_DB_USER: *DB_USER # совпадает с  MYSQL_USER
      WORDPRESS_DB_PASSWORD: *DB_PASSWORD # MYSQL_PASSWORD
      WORDPRESS_DB_NAME: *DB_NAME # MYSQL_DATABASE
    volumes:
      - ./wordpress:/var/www/html # Монтируем папку контейнера /var/www/html
                                  # в локальную папку по пути ./wordpress

```

---

### Сохраним (но не сильно) папку home
```yml
version: '3'

volumes: # Обьявляем о существовании томов
  home-dir: # Название вольюма

services:
  wordpress:
    image: wordpress
    ...
    volumes:
      - ./wordpress:/var/www/html
      - home-dir:/root # Монтируем папку контейнера /root
                       # в локальную docker volume home-dir
```

---

### Монтирование только для чтения 

```yml
    volumes:
      - ./wordpress:/var/www/html:ro
```

### Явно разрешить запись

```yml
    volumes:
      - ./wordpress:/var/www/html:rw
```

---

### Где будет храниться остальное?

---

### Добавим общую сеть 

По умаолчанию сеть default

```yml
version: '3'

# Блок networks в начале или конце файла
networks: # Начало описание сетей
  backend-network: # Название сети
    driver: bridge # Тип сети

services:
  &DB_HOST db:
    image: mysql:5.7
    ...
    networks: # Контейнер подключить к сети
      - backend-network # с названием `backend-network`
  wordpress:
    image: wordpress
    ...
    ports:
      - 8888:80
    networks:
      - backend-network
  adminer:
    image: adminer
    ...
    ports:
      - 8080:8080
    networks:
      - backend-network
```

---

### docker-compose поддерживает .env

```yml
services:

  &DB_HOST db:
    image: mysql:5.7
    restart: always
    environment:
      MYSQL_USER: &DB_USER exampleuser # задаем переменную DB_USER
      MYSQL_PASSWORD: &DB_PASSWORD examplepass # --//--
      MYSQL_DATABASE: &DB_NAME exampledb # == WORDPRESS_DB_NAME
      MYSQL_RANDOM_ROOT_PASSWORD: '1' # рандомный рут пароль
    env_file:
      - .env
```


### Содержимое .env

```conf
DB_USER=exampleuser
DB_PASSWORD=examplepass
DB_NAME=exampledb
```

```yml
services:

  &DB_HOST db:
    image: mysql:5.7
    restart: always
    environment:
      MYSQL_USER: &DB_USER ${DB_USER} # задаем переменную DB_USER
      MYSQL_PASSWORD: &DB_PASSWORD ${DB_PASSWORD} # --//--
      MYSQL_DATABASE: &DB_NAME ${DB_PASSWORD} # == WORDPRESS_DB_NAME
      MYSQL_RANDOM_ROOT_PASSWORD: '1' # рандомный рут пароль
    env_file:
      - .env
```

---

### Добавляйте .env в .gitignore !!!

### Создайте .env.example

---

### Опции описания сетей

```yml
networks: # Начало описание сетей
  1-network: # Название сети
    driver: bridge # Тип сети
  2-network: # Название сети
    external: # Сеть существует
```

Создать сеть

```shell
  docker network create 2-network
```

---


### Кастомим имедж

```yml
version: '3'
services:
  webapp:
    build: # Говорим сбилдить имедж
      context: ./dir # Контекст сборки
      dockerfile: Dockerfile-alternate # Особое имя Dockerfile
      args: # аргументы для docker build
        buildno: 1 # Номер билда
    image: vinz/mysql:latest # обьявляем имя имеджа
```

---

# ДЗ

---

### Добавить docker-compose.yaml для локальной разработки в ваш проект

---

### Применяемыe команды

```shell
docker-compose pull # скачать все имеджи для сервисов

docker-compose build # собрать имеджи для сервисов

docker-compose build up -d --build # Пересобрать и поднять сервисы

docker-compose build up -d --no-build # Поднять сервисы без перезборки

docker-compose push # отправить собранные контейнеры на docker hub

```
---

# ДЗ

---

* Добавить `docker-compose.yml` в ваш текущий проект

Примеры:

https://gitlab.requestum.com/valerii.kravets/symfony-api-edition/tree/docker-compose

https://gitlab.com/viniokil/symfony-in-docker

---

## docker для локальной разработки symfony

---

https://gitlab.requestum.com/valerii.kravets/symfony-api-edition


https://gitlab.com/viniokil/symfony-in-docker

---

![win.jpeg](images/win.jpeg)
