### Docker для продолжающих

<small>Created by [Valerii Kravets](viniokil@gmail.com) / [@viniokil](https://t.me/viniokil)</small>

---
<img src="images/gitlab-logo-gray-stacked-rgb.png"  height="50%" width="50%">

## CI/CD

---

# ДЗ

---

- Повторить пример из занятия
- Добавить полный CI цикл в ваше приложение на Symfony

---

![questions-2.jpeg](../images/questions-2.jpeg)

---

## Что такое CI/CD

---

### Зачем нужен Continuous Integration

---

### Какая разница между Continuous Delivery и Continuous Deployment

---

## Стоит ли деплоить без тестов?

---

<img src="images/angular.png" height="50%" width="50%">

## CI для Angular

---

### Шаг 1

Берем наш базовый проект

```

git clone git@gitlab.com:viniokil/angular-ci-2.git
cd angular-ci-2.git

```

---

### Шаг 2

Смотрим README.md, где указанно:

1. Как запустить проект локально
2. Как установить зависимости
3. Как проверить, что проект работает
4. Как запустить тесты
5. Как собрать проект

---

### Шаг 3

На основании этих данным строим структуру CI:

1. Установка пакетов
2. Запусков тестов
3. Сборка проекта
4. Доставка / деплой

---

### Шаг 4

На основании этих данным строим структуру CI:

`.gitlab-ci.yml`

```yaml
stages:
  - install
  - test
  - build
  - CD
```

----

### Шаг 5

Нужно оперделить какая среда сборки нам нужна. В **package.json** находим:
- `"@angular/core": "~8.2.9"`
- `"@angular/cli": "~8.3.8"`

На просторах docker hub находим docker image `trion/ng-cli:8.3.8`

---

### Шаг 5

Добавляем в gitlab-ci.yml

```yaml
# Docker image for run scripts on job
image: trion/ng-cli:8.3.8

```
---

### Проверяем совместимость и работоспособность
`.gitlab-ci.yml`
```yaml
# GitLab CI help:      <https://docs.gitlab.com/ee/ci/yaml/>
# GitLab environments: <https://docs.gitlab.com/ee/ci/variables/>

# Docker image for run scripts on job
image: trion/ng-cli:8.3.8

stages:
  - install
  - test
  - build
  - CD

Install:
  stage: install
  script: yarn
```

```shell
git commit -am "Addded installation to .gitlab-ci.yml"
git push
```

---

<video controls>
  <source src="images/10-gitlabci-5.mp4" type="video/mp4">
</video>

---

### Шах 6

Задача установки пакетов занимает:
00:01:35

Оптимизируем это кешами:
```yaml
# Use cache
cache:
  # Cache only dir node_modules/
  paths:
    - node_modules/
  # Cache name = branch or tag name
  key: ${CI_COMMIT_REF_NAME}
```
---

```
git commit -am "Addded cache to .gitlab-ci.yml"
git push
```

---

Мы получили время **00:01:12**. Установка занимает минимум вермени. 
Большую часть занимает скачивание и аплоад кеша

---

### Шаг 7

Добавим запуск тестов: 

```yaml
# ------------------------------------------------------------------------------

Lint:
  stage: test
  script: yarn lint

# Karma:
#   image: trion/ng-cli-karma:8.3.8
#   stage: test
#   script: npx ng test
```

```shell
git commit -am "Addded tests to .gitlab-ci.yml"
git push
```

---

<video controls>
  <source src="images/10-gitlabci-7.mp4" type="video/mp4">
</video>

---

### Шаг 8

Собираем приложение
```yaml
# ------------------------------------------------------------------------------

# Name of job
Build static:
  stage: build
  # --prod - use prod config
  # --base-href / - modify base tag (<base href="/">) in index.html
  script: ng build --prod --base-href /
  # set dir `dist/` as artifact
  artifacts:
    # remove after 12 hour
    expire_in: 12 hour
    paths:
    - dist/
```

---

<video controls>
  <source src="images/10-gitlabci-5.mp4" type="video/mp4">
</video>

---

Приложение собираеться не в папку dist. Исправим это

открываем `angular.json`

```json
      "architect": {
        "build": {
          "builder": "@angular-devkit/build-angular:browser",
          "options": {
            // заменяем
            "outputPath": "dist/sampleNG",
            // на
            "outputPath": "dist/",
```

```shell
git commit -am "Сhange angular build output dir"
git push
```
---

### Шаг 9

Деплой на сервер по ssh при помощи **rsync**:

```yaml
# ------------------------------------------------------------------------------

Rsync:
  image: alpine:latest
  stage: CD
  variables:
    SSH_PRIVATE_KEY_B64: $DEV_SSH_PRIVATE_KEY_B64
    SSH_HOST: $DEV_SSH_HOST
    SSH_PORT: $DEV_SSH_PORT
    SSH_USER: $DEV_SSH_USER
    TARGET_DIR: /var/www/html/
    SOURCE_DIR: ./dist/ # Slash `/` available!
  script:
  - apk add openssh-client rsync
  - eval $(ssh-agent -s)
  - echo "$SSH_PRIVATE_KEY_B64" | base64 -d | tr -d '\r' | ssh-add - > /dev/null
  - mkdir -p ~/.ssh
  - chmod 700 ~/.ssh
  - test -f /.dockerenv && echo -e "Host *\n\tStrictHostKeyChecking no\n\n" > ~/.ssh/config
  - rsync -e "ssh -p ${SSH_PORT:-22}" -avz --delete --exclude=.git* --exclude=.docker* -O ${SOURCE_DIR} ${SSH_USER}@${SSH_HOST}:${TARGET_DIR}
  # Disable cached node_modules
  cache: {}
  # Run when DEV_SSH_HOST set
  only:
    variables:
      - $DEV_SSH_HOST != null
```

---

### Настройка пользователя для деплоя
```shell
ssh ssh username@remote_host
sudo useradd -m -s /bin/bash deploy
sudo chown deploy:deploy /var/www/html/
sudo -iu deploy
ssh-keygen -t rsa -N "" -f ~/.ssh/gitlab-deploy
cat ~/.ssh/gitlab-deploy.pub > ~/.ssh/authorized_keys
base64 -w 0 ~/.ssh/gitlab-deploy
```
---

<video controls>
  <source src="images/10-gitlabci-9.mp4" type="video/mp4">
</video>

---

### Шаг 10

Настройка переменных окружения:

---

| Название перменной      | Пример        |
| ----------------------- | ------------- |
| DEV_SSH_PRIVATE_KEY_B64 | LS0tLS1CRUdJTiBSU0EgUFJJVkFURSBLRVktLS0tLQpNSUlFb3dJQkFBS0NBUUVBdGZLRm1iREZtUkppeEIrY1hKdzJHTDZEbUsrLzlMR2NYUFBnSVBZUjdHZHBRV1BWCm4xZ3ppN2dXcVh2VzlzK3JLRjJIRGp2SURraXBvd0g4OXBhc29qZlZ5YXI5Tk1LZWJITDlxOGRoZ1pXeVRKTmYKT0FQV3NrL1Y3UDVzTktxM0ppaWl5U1ZGaG14TzhDWTdDU1ZQMDAxSTlwbHprZ1pRRVdMQ3p0MUFKN0JqYllNdApEQkxFN1R1L3l0dFM5dU03K3djYWsyd1FsR1REYi9xUkhkcnorcnhHelh5R2N5aU5JNDQzT1lWRzRhTXFJTTM3CnZFK0M1M3ZjZlNVOW1lSUJGeitMZ2RKYzRYTTZlNndVUkd2eFVTUGc0RlpaRHRGNHlqUlBRYUVKYzJ0M0Y2Ym8KMDRCUXdFUy80OXMycG0vbk1aT0pydUZXbW1Xc09tSlZDZjhaZFFJREFRQUJBb0lCQUMyV3Y5ejdpdmRJd1JDTgpMeHVSVklpc3E4S0ZQdDZyNWhid1VGZFI4TG5BUzRKRDVwSFBndExlWkdkQkRXSzROQldnSWFmeVdMVWpoY1cvClNNZnNod0w2Wkluc1d4ZHVNRTNTSGpkOWRMUUljeTEydTZRSWFqSXhpTk9pQ0RER0dhL28vS2pUdXZEc2tIYmgKeldjRVAxVWRoMHVxdFdlQVIyMmp4bFlZMyt0UmZhVjJrc1RjaldJMHd3eXBLeXFJMjZxUEN0YnRLVjVWT1kzawpnb1hGNFFmWXVzN0tIN2N5cVlObkJOdlhVWnJXY2IzaUdGQVJtU2R0c25kRFRpL2hIZGYrMkdIeEhTb3MrUmZMCjJvR1pxYWJCRXduK2RieGhacS9JMGZqSStRR21GOG0zRWMyZno2K0pnZnR3dFZXRW1hN3dwVnp4U2V0ME1nRFIKTTBINEM0RUNnWUVBNW16UnF3ejlZSUxtK29BS25xZUxkRVM1dkJkdHVLV0RqZUV0Q3YvbTUyTlZMYWZjbDd4QQpxOHZoeFpjS3NMUUYrU1pFZUlSRVA3ZTFjcFdudFpwQzBHbFVneEFtV0xMS1MzUGFHaTZYbm9XMlFnWUhLUElWCmdYUGEzK29uNmZBa2lJWFZuckozamlDanV4cFhsT2dtRDM1cGJVZThCYkVLTnlZdzlQUzV1ZUVDZ1lFQXlpUkkKRGdoaXVybG1lZ2lpRi9MdEhKT1NuTndPelQwVm43VDNORndXaFhLaFVOOVUyQnF3cm1qY1lPQzU0OHdkWEpBSAp2WFczOW5RSGpLdU5HVEx3VHVRQkFqb2VKY1lVMTVKT0lSbDFqSlJhanJOZnM2VlVjdW9wODhSK1VWZjdrR1VICndKSEpIaTFPVm9KNThELy9IaGhPVWtHb1liQ3Z3b05nU0tvaUdoVUNnWUVBZ1RVeDJOV2UydGhzUnJrM3UwRTkKN0lPQkZ0eVlWUHJTcXVOQ1M3RzFPN2l5a2JoWDlPbkY1YytjbVJ5YURQM1ZmNkVhc2UyUkM1MnE3RWJJMVFkYwpRZHRlWFdQWDdOTE1wc1NlM3lHVE9YdkcrSHpDMVEzeVN6VzQzT2hEVzJXaVJyK3RTcURPYzI4UUFLSER6S2JrCmVMbXN5bTk0R2tUczhvSEM0QWkrZ0dFQ2dZQnpBc0djZWxqeU5KcUJ4SGUxZUpwQU1CYXkrT0hURkhaQk1BMlcKMU81YmJ5VXBobWdYck5mcDQrKy9qeDAwNXVXdHpLVWF2dWUwYWNBQjRYbWVaTVVsbmNTVko4d1BZZXhwMnV4cApHTHN4Q1JlZEthSEI3TWRudXFPejlwQ0laNjdTTVNsLy8zS1dvT0I1aURNbzVzQ1l0dXVaRVpKa1Fzczl1TklICmhnVnhJUUtCZ0YvZm9HeVZod3dHUE1jT1FUNE1aLzBRSDQrSnYybnpOU0FLeEkxVkN3M2RBNDhiL3RTTXhhRG4KRzN6K09za0UrOVZsekl6Syt6Ti9sZTJCV3BJTGEwZmVuMUhNblhUY2Q0d0hsNldNdit0SkRBbzhDNjl2Nzl4RwpCS091R3FFOHdXTXBSbTl3VjlBaTZvdHpFUDRJUkthZGs3cDNEeGNWWkUwVW9aYU4wY3Q0Ci0tLS0tRU5EIFJTQSBQUklWQVRFIEtFWS0tLS0tCg== |
| DEV_SSH_HOST            | 34.90.139.129 |
| DEV_SSH_PORT            | 22            |
| DEV_SSH_USER            | deploy        |

---

<video controls>
  <source src="images/10-gitlabci-10.mp4" type="video/mp4">
</video>

---

<video controls>
  <source src="images/10-gitlabci-10-2.mp4" type="video/mp4">
</video>

---

### Шаг 11

Сборка docker image

---

Добавляем в проект файл

`docker/nginx/Dockerfile`
```Dockerfile
FROM nginx:alpine

COPY docker/nginx/default.conf /etc/nginx/conf.d/default.conf

COPY ./dist/ /usr/share/nginx/html/
```

---

Добавляем в проект файл

`docker/nginx/default.conf`
```conf
server {
  listen 80;
  location / {
    root /usr/share/nginx/html;
    index index.html index.htm;
    try_files $uri $uri/ /index.html =404;
  }
}
```

---
Добавляем в `.gitlab-ci.yml`

```yml
Docker:
  image: docker:stable
  stage: CD
  services:
  - docker:dind
  # Tag docker image as branche name
  variables:
    TAG: ${CI_COMMIT_REF_NAME}
    DOCKER_DRIVER: overlay2
  script:
  - docker login -u gitlab-ci-token -p "$CI_JOB_TOKEN" "$CI_REGISTRY"
  # Build app
  - docker build -t ${CI_REGISTRY_IMAGE}:${TAG} -f ./docker/nginx/Dockerfile ./
  - docker push ${CI_REGISTRY_IMAGE}:${TAG}
```

---

<video controls>
  <source src="images/10-gitlabci-11.mp4" type="video/mp4">
</video>

---
### Шаг 12

Деплой в S3


```yaml

S3:
  image: python:latest
  stage: CD
  variables:
    S3_BUCKET_NAME: "angular.dev-ops.me"
    S3_REGION: eu-central-1
  # AWS_ACCESS_KEY_ID and AWS_SECRET_ACCESS_KEY set in
  # Gitlab CI secret variables
  # https://docs.gitlab.com/ee/ci/variables/#via-the-ui
  environment:
    name: review
    url: https://s3.${S3_REGION}.amazonaws.com/${S3_BUCKET_NAME}/${CI_BUILD_REF_NAME}/
  script:
  - pip install awscli
  - aws s3 cp ./dist/ s3://${S3_BUCKET_NAME}/${CI_BUILD_REF_NAME}/ --recursive
  # Disable cached node_modules
  cache: {}
  only:
    variables:
      - $AWS_ACCESS_KEY_ID != null
      - $AWS_SECRET_ACCESS_KEY != null
    refs:
      - master
```

---

Добавляем секретные переменные:
- `AWS_ACCESS_KEY_ID`
- `AWS_SECRET_ACCESS_KEY`

---

<video controls>
  <source src="images/10-gitlabci-12.mp4" type="video/mp4">
</video>

---

# ДЗ 

---

![nodz.jpg](../images/nodz.jpg)

